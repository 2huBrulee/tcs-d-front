import React, { Component } from 'react';
import {Panel} from 'react-bootstrap';
import {Grname, Col, Row} from 'react-bootstrap';
import {FormGroup, ControlLabel, FormControl, FieldGroup} from 'react-bootstrap';
import {Button} from 'react-bootstrap';

const SemesterSearchForm = ({handleChange=f=>f,onClick=f=>f,...props}) =>
    <div>
        <Panel bsStyle="primary">
            <Panel.Heading>
                <Panel.Title componentClass="h3">Consultas</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
                <form className="ciclo-form" >
                    <Row>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Semestre</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="semestre"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Curso</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="curso"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Docente</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="docente"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Dia</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="dia"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Hora Inicio</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="hinicio"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                                <ControlLabel>Hora Final</ControlLabel>
                                <FormControl className="color-fondo"
                                            name="hfin"
                                             type="text"
                                             onChange={handleChange}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Button bsStyle="primary" onClick={onClick}>Buscar</Button>
                </form>
            </Panel.Body>
        </Panel>
    </div>
export default SemesterSearchForm