import React, { Component } from 'react';
import {Panel, Button} from 'react-bootstrap';
import PhotoPanel from './PhotoPanel';
import {FormGroup, ControlLabel, FormControl, FieldGroup} from 'react-bootstrap';
import SemesterSearchForm from "./SemesterSearchForm";
import PanelResultado from "./PanelResultado";
import dataB from '../initialData/dataConsultas';
import { compose } from 'redux'
import { OffCanvas, OffCanvasMenu, OffCanvasBody } from 'react-offcanvas';
import styles from './styles.css'
import InfAdicionalModel1 from "./InfAdicionalModel1";
import InfAdicionalModel2 from "./InfAdicionalModel2";

class ModuloConsultas extends Component {
    constructor(...props){
        super(...props)
        this.state = {
            isMenuOpened: false,
            cData: dataB,
            resultados: [],
            filteredList:[],
            nameFilter: '',
            courseFilter: '',
            dispoFilter: '',
            disponibilidadFilter:'',
            phoneFilter:'',
            dniFilter:'',
            lastnameFilter:'',
            search:{
                semestre:'',
                curso:'',
                docente:'',
                dia:'',
                hinicio:'',
                hfin:'',
            },
        }

        this.handleFilter = this.handleFilter.bind(this);
        this.searchDB = this.searchDB.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.filterByLastName = this.filterByLastName.bind(this);
        this.filterByName = this.filterByName.bind(this);
        this.filterList = this.filterList.bind(this);
        this.filterByDisp = this.filterByDisp.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.showInfoDocente = this.showInfoDocente.bind(this);
        
    }

    showInfoDocente = () =>
        this.handleClick()

    handleClick = () =>
        this.setState({ isMenuOpened: !this.state.isMenuOpened });

    filterByName = (initialList) =>
        initialList.filter(datum=>(datum.nombres.toLowerCase().search(this.state.nameFilter)!==-1));      
    
    filterByLastName = (initialList) =>
        initialList.filter(datum=>(datum.apellidos.toLowerCase().search(this.state.lastnameFilter)!==-1));
        
    filterByDNI = (initialList) =>
        initialList.filter(datum=>(datum.dni.toLowerCase().search(this.state.dniFilter)!==-1));      
    
    filterByPhone = (initialList) =>
        initialList.filter(datum=>(datum.phone.toLowerCase().search(this.state.phoneFilter)!==-1));      
        
    filterByDisp = (initialList) =>
        initialList.filter(datum=>(datum.semestre[0].disponibilidad.reduce((acc,curr)=>curr.nombre.toLowerCase().search(this.state.dispoFilter)!==-1||acc,false)));

    filterList = 
        compose(
            this.filterByDisp,
            this.filterByPhone,
            this.filterByDNI,
            this.filterByLastName,
            this.filterByName
        );

    handleChange = (e) => {
        e.preventDefault;
        e.persist();
        console.log(e.target)
        console.log(e.target.name)
        console.log(e.target.value)

        let property = e.target.name;
        this.setState(prevState => {
            const values = {[property]:e.target.value}
            const newSearch = Object.assign(prevState.search,values);
            this.setState({search:newSearch})
        })
    }

    searchDB = () => 
        Object.values(this.state.search).map(n=>window.alert(n))
        
    handleFilter = (e) => {
        e.preventDefault;
        console.log(e.target)
        let property = e.target.name;
        this.setState({[property]: e.target.value})
    }

    componentWillMount(){
        this.setState({resultados:this.state.cData})
    }

    render() {
        console.log(this.state.dispoFilter)
        const resultados = this.filterList(this.state.resultados);
        const {handleFilter, handleChange,searchDB} = this;
        return (

            <OffCanvas width={300} transitionDuration={300} isMenuOpened={this.state.isMenuOpened} position={"right"}>
                <OffCanvasBody className={styles.bodyClass} style={{fontSize: '30px'}}>
                    <h2>Modulo Consultas</h2>
                    <SemesterSearchForm onClick={searchDB} handleChange={handleChange}/>
                    <PanelResultado showDocenteInfo={this.showInfoDocente} handleChanges={handleFilter} resultados={resultados} />
                </OffCanvasBody>
                <OffCanvasMenu className={styles.menuClass}>
                    <Panel bsStyle="primary" className={"infoClass"}>
                        <Panel.Heading>
                            <Panel.Title componentClass="h3">Informacion Adicional</Panel.Title>
                        </Panel.Heading>
                        <Panel.Body>
                            <div>
                                <PhotoPanel/>
                                <InfAdicionalModel1/>
                            </div>
                        </Panel.Body>
                    </Panel>
                </OffCanvasMenu>
            </OffCanvas>

        )
    }
}
    
export default ModuloConsultas
