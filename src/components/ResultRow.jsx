import React, { Component } from 'react';

const getDisponibilidad = (disponibilidades) => {
    let disponibilidad = '';
    disponibilidades.map(n=> disponibilidad=disponibilidad + n.nombre + ' ' + n.hinicio + ' a ' + n.hfin + ' ')
    return disponibilidad
}

const ResultRow = ({resultado,onClick,...props}) =>
    <tr onClick={onClick}>
        <td>{resultado.nombres}</td>
        <td>{resultado.apellidos}</td>
        <td>{resultado.dni}</td>
        <td>{resultado.phone}</td>
        <td>{getDisponibilidad(resultado.semestre[0].disponibilidad)}</td>
    </tr>

export default ResultRow;