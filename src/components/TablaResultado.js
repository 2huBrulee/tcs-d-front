import React from 'react';
import {Table, FieldGroup} from 'react-bootstrap';
import ResultRow from './ResultRow';
import { FormControl } from 'react-bootstrap';

const TableResultado = ({resultados,filterHandler=f=>f,getDocenteData=f=>f,...props}) =>
    <div>
        <Table>
            <Table striped bordered condensed hover responsive>
                <thead>
                <tr>        
                    <th className="text-center">Nombre
                        <FormControl className="color-fondo"
                                            name='nameFilter'
                                             type="text"
                                             onChange={filterHandler}/>
                    </th>
                    <th className="text-center">Apellido
                        <FormControl className="color-fondo"
                                            name='lastnameFilter'
                                             type="text"
                                             onChange={filterHandler}/>
                    </th>
                    <th className="text-center">DNI
                        <FormControl className="color-fondo"
                                            name='dniFilter'
                                             type="text"
                                             onChange={filterHandler}/>
                    </th>
                    <th className="text-center">Celular
                        <FormControl className="color-fondo"
                                            name='phoneFilter'
                                             type="text"
                                             onChange={filterHandler}/>
                    </th>
                    <th className="text-center">Disponibilidad
                        <FormControl className="color-fondo"
                                            name='dispoFilter'
                                             type="text"
                                             onChange={filterHandler}/>
                    </th>
                </tr>
                </thead>
                <tbody>
                        {(resultados.length>0)?resultados.map((n,i)=>
                            <ResultRow onClick={getDocenteData} key={i} resultado={n}/>) : <div></div>
                        }
                </tbody>
            </Table>
        </Table>
    </div>
export default TableResultado;